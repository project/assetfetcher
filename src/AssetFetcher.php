<?php

namespace Drupal\assetfetcher;

use Drupal\assetfetcher\SRI\SubResourceIntegrityCheckerInterface;
use Drupal\assetfetcher\SRI\SubResourceIntegrityException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StreamWrapper\LocalStream;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use SplFileInfo;

/**
 * Class AssetFetcher
 *
 * @internal
 */
class AssetFetcher implements AssetFetcherInterface {

  use StringTranslationTrait;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private $logger;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  private $streamWrapperManager;

  /**
   * The AssetFetcher config.
   *
   * @var \Drupal\assetfetcher\AssetFetcherConfigInterface
   */
  private $assetFetcherConfig;

  /**
   * The SRI checker.
   *
   * @var \Drupal\assetfetcher\SRI\SubResourceIntegrityCheckerInterface
   */
  private $integrityChecker;

  /**
   * AssetFetcher constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   * @param \Drupal\assetfetcher\AssetFetcherConfigInterface $assetFetcherConfig
   */
  public function __construct(FileSystemInterface $fileSystem, LoggerChannelFactoryInterface $loggerFactory, StreamWrapperManagerInterface $streamWrapperManager, AssetFetcherConfigInterface $assetFetcherConfig, SubResourceIntegrityCheckerInterface $integrityChecker) {
    $this->fileSystem = $fileSystem;
    $this->logger = $loggerFactory->get('assetfetche');
    $this->streamWrapperManager = $streamWrapperManager;
    $this->assetFetcherConfig = $assetFetcherConfig;
    $this->integrityChecker = $integrityChecker;
  }

  /**
   * Adjust CSS or JS sources.
   *
   * @param array $sources
   *   The css or js array.
   * @param string $fileType
   *   The file type, css or js.
   */
  public function adjustSources(array &$sources, string $fileType) {
    $enabled = $this->assetFetcherConfig->enabled();
    foreach ($sources as &$info) {
      // Check asset type AND uri. Some modules like webform declare
      // a library as external, then in an alter swap the Uri out against a
      // root-relative one.
      // @todo Research if other inconsistencies can make assets sneak through.
      $assetType = $info['type'] ?? '';
      $assetTypeIsExternal = $assetType === 'external';
      $assetUri = $info['data'];
      // Make it broad and match any scheme, to also get s3: and whatever.
      // Also match '//foo.com/bar' which is a valid url.
      $assetUriIsExternal = strpos($assetUri, '//') !== FALSE;
      if ($assetTypeIsExternal && $assetUriIsExternal) {
        $integritySpec = $info['attributes']['integrity'] ?? NULL;
        $localPathOrUri = '';
        $message = NULL;
        if ($enabled) {
          // If applicable, try first to get the unminified version, for better
          // debugging DX. On production, we should have aggregation and
          // minification in place anyway.
          try {
            if ($this->assetFetcherConfig->preferUnminified() && ($unminifiedUri = preg_replace('~\.min(\.js|\.css)$~', '$1', $assetUri)) !== $assetUri) {
              try {
                $localPathOrUri = $this->fetch($unminifiedUri, $integritySpec, $fileType);
                $info['minified'] = FALSE;
              } catch (\Throwable $e) {
                // OK, we could not retrieve the guessed unminified url.
                // Try the original url.
              }
            }
            if (empty($localPathOrUri)) {
              $localPathOrUri = $this->fetch($assetUri, $integritySpec, $fileType);
            }
          } catch (\Throwable $e) {
            $message = $e->getMessage();
            $this->logger->error($message);
          }
        }

        if ($localPathOrUri) {
          // For now, we have to convert this to a path relative to Drupal root,
          // until https://www.drupal.org/project/drupal/issues/2735717 is fixed.
          // While in .libraries.yml files, we have to prepend a / to drupal root
          // relative files, at this point all paths ar relative to drupal root
          // and without a leading /.
          // @see \Drupal\Core\Asset\LibraryDiscoveryParser::buildByExtension
          $rootRelativePath = $this->toRootRelativePath($localPathOrUri);
          // No need to swap out the key, only data is used.
          $info['assetfetcher_could_fetch'] = $assetUri;
          $info['data'] = $rootRelativePath;
          $info['type'] = 'file';
        }
        elseif ($enabled) {
          // Mark this file as invalid.
          $info['assetfetcher_could_not_fetch'] = $assetUri;
          if ($message) {
            $info['assetfetcher_message'] = $message ?? $this->t('Mysterious error.');
          }
          if (!$this->assetFetcherConfig->allowRemote()) {
            // Replace by an invalid local file name for now.
            $adjustedRemoteUri = str_replace('://', '|', $assetUri);
            $info['type'] = 'file';
            $info['data'] = "--assetfetcher-could-not-fetch/$adjustedRemoteUri";
          }
        }
        else {
          $info['assetfetcher_could_not_fetch'] = $assetUri;
          $info['assetfetcher_message'] = $this->t('Asset Fetcher is not enabled.');
        }
      }
    }
  }

  /**
   * Fetch an external asset and return its local file uri.
   *
   * @todo Consider deleting these files on cache rebuild.
   *
   * @param string $uri
   *   The asset's uri.
   * @param $integritySpec
   *   The SRI spec like used in the integrity attribute.
   * @param $fileType
   *   The file type, css or js.
   *
   * @return string
   *   The local asset's uri with the public:// stream wrapper, or a
   *   root-relative local path without leading /.
   */
  private function fetch($uri, $integritySpec, $fileType) {
    foreach ($this->parseLibraryPaths($uri) as $libraryPath) {
      $librariesRootRelativePath = "libraries/$libraryPath";
      if (file_exists($librariesRootRelativePath)) {
        return $librariesRootRelativePath;
      }
    }

    $localPath = $this->mapUriToLocalPath($uri);

    $localUri = "public://$localPath";
    if (!is_file($localUri)) {
      // Use a tmp name first and only rename when checked.
      $localTmpUri = $this->fileSystem->tempnam($this->fileSystem->getTempDirectory(), 'assetfetcher');
      // This is passed by ref, so create a variable.
      $localTmpUriDir = $this->fileSystem->dirname($localTmpUri);
      $this->fileSystem->prepareDirectory($localTmpUriDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      /** @var string|false $localUri */
      $localTmpUri = system_retrieve_file($uri, $localTmpUri, FALSE, FileSystemInterface::EXISTS_REPLACE);
      if (!$localTmpUri) {
        throw new \RuntimeException("Can not retrieve uri $uri.");
      }
      /** @var string $localTmpUri */
      if ($integritySpec) {
        try {
          $this->integrityChecker->checkFile($integritySpec, $localTmpUri);
        } catch (\Throwable $e) {
          // Never leave invalid files.
          $this->fileSystem->delete($localTmpUri);
          throw new \RuntimeException("SRI error for $uri: {$e->getMessage()}", 0, $e);
        }
      }
      // @todo Adjust urls in CSS.
      $localUriDir = $this->fileSystem->dirname($localUri);
      $this->fileSystem->prepareDirectory($localUriDir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $this->fileSystem->move($localTmpUri, $localUri, FileSystemInterface::EXISTS_REPLACE);
    }
    return $localUri;
  }

  public function parseLibraryPaths(string $url): array {
    $parts = parse_url($url);
    $cdnHost = $parts['host'];
    $parts = explode('/', $parts['path']);
    switch ($cdnHost) {
      case 'ajax.googleapis.com':
      case 'cdnjs.cloudflare.com':
        // https://ajax.googleapis.com/ajax/libs/d3js/7.4.2/d3.min.js
        // https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.32/vue.cjs.js
        [,, $library, $version] = $parts;
        $path = implode('/', array_slice($parts, 4));
        break;
      case 'ajax.aspnetcdn.com':
        if (count($parts) > 3) {
          // https://ajax.aspnetcdn.com/ajax/jquery.cycle/2.99/jquery.cycle.all.js
          [, $library, $version] = $parts;
          $path = implode('/', array_slice($parts, 3));
        }
        else {
          // ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js
          // @todo Support this if needed.
          return [];
        }
        break;
      case 'cdn.jsdelivr.net':
        // https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js
        [$library, $version] = explode('@', $parts[1]) + [1 => ''];
        $path = implode('/', array_slice($parts, 2));
        break;
      case 'unpkg.com';
        // https://unpkg.com/react@16.7.0/umd/react.production.min.js
        [$library, $version] = explode('@', $parts[0]) + [1 => ''];
        $path = implode('/', array_slice($parts, 1));
        break;
      default:
        return [];
    }

    $libraryPaths = [];
    if ($version) {
      $libraryPaths[] = "$library@$version/$path";
    }
    $libraryPaths[] = "$library/$path";
    return $libraryPaths;
  }

  /**
   * Make a local uri for an external asset.
   *
   * This maps external uris to a flat public://dir/filename.
   *
   * @param string $uri
   *   The external uri.
   *
   * @return string
   *   The mapped path.
   */
  private function mapUriToLocalPath($uri) {
    $parts = parse_url($uri);
    $fileParts = array_intersect_key($parts, ['path'=> 0, 'query' => 0, 'fragment' => 0]);
    // A subset of: schema, host, port, user, pass.
    $dirParts = array_diff_key($parts, $fileParts);

    // Keys: dirname, basename, filename, extension
    $pathInfo = pathinfo($parts['path']);

    // Flatten the directory path to one level if exists.
    if ($pathInfo['dirname'] === '.') {
      unset($dirParts['path']);
    }
    else {
      $dirParts['path'] = strtr($pathInfo['dirname'], ['/' =>'--']);
    }

    // Ensure extension is after query and fragment.
    $fileParts['path'] = $pathInfo['filename'];
    $extension = $pathInfo['extension'] ?? 'txt';


    // Leverage that parse_url returns parts in the right order.
    $dirName = implode('--', $dirParts);
    $fileName = implode('--', $fileParts);
    return "assetfetcher/$dirName/$fileName.$extension";
  }

  /**
   * Convert a local file uri to a root relative path.
   *
   * @param string $localUri
   *
   * @return string
   */
  private function toRootRelativePath(string $localUri) {
    $wrapper = $this->streamWrapperManager->getViaUri($localUri);
    if ($wrapper instanceof LocalStream) {
      $publicPath = $wrapper->getDirectoryPath();
      $targetPath = $this->streamWrapperManager->getTarget($localUri);
      $localPath = "$publicPath/$targetPath";
      return $localPath;
    }
    else {
      return $localUri;
    }
  }

}
