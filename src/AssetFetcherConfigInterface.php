<?php

namespace Drupal\assetfetcher;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface AssetFetcherConfigInterface
 *
 * @internal
 */
interface AssetFetcherConfigInterface {

  public function enabled();

  public function preferUnminified();

  public function allowRemote();

  public function hookFormAlter(&$form, FormStateInterface $formState, $formId);
}