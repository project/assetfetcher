<?php

namespace Drupal\assetfetcher;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class AssetFetcherConfig
 *
 * @internal
 */
class AssetFetcherConfig implements AssetFetcherConfigInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * AssetFetcherConfig constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  public function enabled() {
    return $this->configFactory->get('assetfetcher.settings')->get('enabled');
  }

  public function preferUnminified() {
    return $this->configFactory->get('assetfetcher.settings')->get('prefer_unminified');
  }

  public function allowRemote() {
    return $this->configFactory->get('assetfetcher.settings')->get('allow_remote');
  }

  public function hookFormAlter(&$form, FormStateInterface $formState, $formId) {
    if ($formId === 'system_performance_settings') {
      $config = $this->configFactory->getEditable('assetfetcher.settings');
      $form['bandwidth_optimization']['assetfetcher'] = [
        '#tree' => TRUE,
        '#title' => $this->t('Asset Fetcher'),
        '#type' => 'fieldset',
        '#open' => TRUE,
      ];
      $form['bandwidth_optimization']['assetfetcher']['enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable fetching assets'),
        '#description' => $this->t('All remote assets will be copied and served as local files. This does not cover css includes like fonts.'),
        '#default_value' => $config->get('enabled'),
      ];
      $form['bandwidth_optimization']['assetfetcher']['prefer_unminified'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Prefer unminified assets'),
        '#description' => $this->t('This can help with debugging.'),
        '#default_value' => $config->get('prefer_unminified'),
      ];
      $form['bandwidth_optimization']['assetfetcher']['allow_remote'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Allow remote assets if fetching fails'),
        '#description' => $this->t('If you use this for GDPR compliance, you will prefer an error.'),
        '#default_value' => $config->get('allow_remote'),
      ];
      $form['actions']['submit']['#submit'][] = [$this, 'submitForm'];
    }
  }

  public function submitForm(array &$form, FormStateInterface $formState) {
    $this->configFactory->getEditable('assetfetcher.settings')
      ->set('enabled', $formState->getValue(['assetfetcher', 'enabled']))
      ->set('prefer_unminified', $formState->getValue(['assetfetcher', 'prefer_unminified']))
      ->set('allow_remote', $formState->getValue(['assetfetcher', 'allow_remote']))
      ->save();
    // Only optimizer info will be cleared in form controller
    // @see \Drupal\system\Form\PerformanceForm::submitForm
    // But our asset alterations will still be cached:
    // @see \Drupal\Core\Asset\AssetResolver::getCssAssets
    // @see \Drupal\Core\Asset\AssetResolver::getJsAssets
    Cache::invalidateTags(['library_info']);
  }

}
