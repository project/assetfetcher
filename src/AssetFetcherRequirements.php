<?php

namespace Drupal\assetfetcher;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AssetFetcherRequirements
 *
 * @internal
 */
class AssetFetcherRequirements implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * @var \Drupal\assetfetcher\AssetFetcherInterface
   */
  protected $assetFetcher;

  /**
   * AssetFetcherRequirements constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $libraryDiscovery
   * @param \Drupal\assetfetcher\AssetFetcherInterface $assetFetcher
   */
  public function __construct(ModuleHandlerInterface $moduleHandler, ThemeHandlerInterface $themeHandler, LibraryDiscoveryInterface $libraryDiscovery, AssetFetcherInterface $assetFetcher) {
    $this->moduleHandler = $moduleHandler;
    $this->themeHandler = $themeHandler;
    $this->libraryDiscovery = $libraryDiscovery;
    $this->assetFetcher = $assetFetcher;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('theme_handler'),
      $container->get('library.discovery'),
      $container->get('assetfetcher.service')
    );
  }

  public function runtimeRequirements() {
    // @todo Handle gracefully if assetfetcher is not enabled.
    $couldFetch = [];
    $couldNotFetch = [];
    $extensions = ['core' => 'DUMMY']
      + $this->moduleHandler->getModuleList()
      + $this->themeHandler->listInfo();
    foreach ($extensions as $extensionName => $extension) {
      $libraries = $this->libraryDiscovery->getLibrariesByExtension($extensionName);
      foreach ($libraries as $libraryName => $library) {
        foreach (['js' => $library['js'] ?? [], 'css' => $library['css'] ?? [],] as $fileType => $items) {
          $this->assetFetcher->adjustSources($items, $fileType);
          foreach ($items as $item) {
            if ($remote = $item['assetfetcher_could_fetch'] ?? NULL) {
              $short = basename($remote);
              $base_path = base_path();
              $couldFetch["$extensionName/$libraryName"][$remote] = "<li><a href=\"$remote\">$short</a> => <a href=\"$base_path$item[data]\">local</a></li>";
            }
            if ($remote = $item['assetfetcher_could_not_fetch'] ?? NULL) {
              $short = basename($remote);
              $couldNotFetch["$extensionName/$libraryName"][$remote] = "<li><a href=\"$remote\">$short</a>: $item[assetfetcher_message]</li>";
            }
          }
        }
      }
    }

    $requirements = [];
    if ($couldNotFetch) {
      $requirements['assetfetcher_could_not_fetch'] = [
        'title' => $this->t('Asset Fetcher'),
        'description' => $this->t('Errors:<br>@list', ['@list' => $this->makeList($couldNotFetch)]),
        'severity' => REQUIREMENT_ERROR,
      ];
    }
    if ($couldFetch) {
      $requirements['assetfetcher_could_fetch'] = [
        'title' => $this->t('Asset Fetcher'),
        'description' => $this->t('Success:<br>@list', ['@list' => $this->makeList($couldFetch)]),
        'severity' => REQUIREMENT_OK,
      ];
    }
    if (!$requirements) {
      $requirements['assetfetcher_nothing_to_do'] = [
        'title' => $this->t('Asset Fetcher'),
        'description' => $this->t('There have been no remote assets to fetch.'),
        'severity' => REQUIREMENT_OK,
      ];
    }
    return $requirements;
  }

  private function makeList(array $fetch) {
    $parts = [];
    // All input is module provided, none user provided, so no sanitizing need.
    foreach ($fetch as $library => $items) {
      $parts[] = "<p>$library</p><ul>";
      foreach ($items as $Line) {
        $parts[] = $Line;
      }
      $parts[] = "</ul>";
    }
    return Markup::create(implode('', $parts));
  }
}
