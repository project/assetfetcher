<?php

namespace Drupal\assetfetcher;

/**
 * AssetFetcherInterface
 *
 * @internal
 */
interface AssetFetcherInterface {

  public function adjustSources(array &$sources, string $fileType);

}
