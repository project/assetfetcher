<?php

namespace Drupal\assetfetcher\SRI;

interface SubResourceIntegrityCheckerInterface {

  /**
   * @param string $integritySpec
   * @param string $fileName
   *
   * @return bool
   */
  public function checkFile(string $integritySpec, string $fileName);

  /**
   * Check SRI of actual data.
   *
   * @param string $integritySpec
   * @param string $data
   *
   * @return bool
   */
  public function check(string $integritySpec, string $data);
}