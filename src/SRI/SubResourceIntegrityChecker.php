<?php

namespace Drupal\assetfetcher\SRI;

class SubResourceIntegrityChecker implements SubResourceIntegrityCheckerInterface {

  /**
   * {@inheritDoc}
   */
  public function checkFile(string $integritySpec, string $fileName) {
    $data = file_get_contents($fileName);
    if ($data === FALSE) {
      throw new SubResourceIntegrityException("File can not be read: $fileName");
    }
    $this->check($integritySpec, $data);
  }

  /**
   * {@inheritDoc}
   */
  public function check(string $integritySpec, string $data) {
    $integrityOptions = explode(' ', $integritySpec);
    $hashValue = '';
    foreach ($integrityOptions as $integrityOption) {
      $parts = explode('-', $integrityOption, 2);
      $hashAlgorithm = $parts[0];
      $validHashMethod = in_array($hashAlgorithm, ['sha256', 'sha384', 'sha512']);
      $hashValueBase64 = $parts[1] ?? '';
      $hashValue = base64_decode($hashValueBase64, TRUE);
      if (count($parts) !== 2 || !$validHashMethod || $hashValue === FALSE) {
        throw new SubResourceIntegrityException("Integrity spec malformed: $integritySpec");
      }

      $actualHashValue = hash($hashAlgorithm, $data, TRUE);
      if ($hashValue === $actualHashValue) {
        return;
      }
    }
    throw new SubResourceIntegrityException("Integrity spec does not match actual data: Spec='$integritySpec', Hash='$hashValue'");
  }

}
